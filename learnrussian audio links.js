// ==UserScript==
// @name     learnrussian audio links
// @namespace   https://learnrussian.rt.com
// @include     *learnrussian.rt.com*
// @version  1
// @grant    none
// @author uldics
// @description My attempt to fix very good language learning sites audio functionality with a userscript. Big THANK YOU to Stackoverflow, as I know almost nothing about Javascript, DOM, html5 and see no difference between old and bannable concepts and too fresh fads.
// ==/UserScript==

//Get conversation players, longer ones, mostly from 5 sec to multiple minutes
//https://learnrussian.rt.com/s/content/lesson4/1.png
///s/content/lesson4/audio/4.1.1.mp3
//<div class="play plpau" audio="/s/content/lesson4/audio/4.1.1.mp3" style="background-color: red;"></div>
var x = document.getElementsByClassName("play plpau");
var i;
for (i = 0; i < x.length; i++) {
	x[i].style.backgroundColor = "red";										//Just so I can see it works
	var lastText = x[i].getAttribute("audio");
	var fullText = 'https://learnrussian.rt.com'+lastText;
	
  var playerLong = document.createElement("audio");
  playerLong.innerHTML = fullText												//Full path for poor people with old browsers, so they can download it manually
	var att1 = document.createAttribute("controls");
	att1.value = true;
	playerLong.setAttributeNode(att1);
	var att2 = document.createAttribute("src");
	att2.value = lastText;																//Relative path seems to work
	playerLong.setAttributeNode(att2);
	x[i].insertAdjacentElement("afterend", playerLong);
	//x[i].remove();	//When removed, only first instance gets the html5 player added, so I leave the clutter until I know what I'm doing
}

//Get short players, 1 sec or less
//<div class="playtwo" audio="/s/content/lesson2/audio/2.1.3.mp3">
//	<div class="plpau"></div>
//	вечер
//</div>
var y = document.getElementsByClassName("playtwo");
var j;
for (j = 0; j < y.length; j++) {
	//y[j].style.backgroundColor = "red";
	var lastText = y[j].getAttribute("audio");
	var fullText = 'https://learnrussian.rt.com'+lastText;
	var strippedText = lastText.replace(/(\/|\.|-)/g,"")+j;		//+j at the end is an unsuccessful attempt to mitigate Lesson 2 Task 2 reload on buttonclick, as same audiofiles are used as in Task 1.
  var btnVal = "document.getElementById('"+strippedText+"').play();"
	console.log(strippedText);
  
  var playerShort = document.createElement("audio");
  playerShort.innerHTML = fullText
	var att1 = document.createAttribute("id");
	att1.value = strippedText;
	playerShort.setAttributeNode(att1);
	var att2 = document.createAttribute("src");
	att2.value = lastText;
	playerShort.setAttributeNode(att2);
	y[j].insertAdjacentElement("afterend", playerShort);
	
	var btn = document.createElement("button");
	var att3 = document.createAttribute("onclick");
	att3.value = btnVal;
  btn.setAttributeNode(att3);
  playerShort.insertAdjacentElement("afterend", btn);
}



