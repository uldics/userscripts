# userscripts

Userscripts to fix issues with some sites. 

## learnrussian audio links
Very well made Russian learning site https://learnrussian.rt.com, which unfortunately does not play audio anymore. This is my Javascript beginner attempt to fix it userside, as the audiofiles are accessible. I have no idea why the original site does not play them. My solution is just MVP, nothing nice looking or highly polished.

What I have done here:
 * detect the links of longer audio, create a html5 player with controls over the top of original player
 * detect the links of shorter audio, create html5 audio tag and a button associated with it

Lesson 2 Task 2 has a bug - when clicking on the buttons, the page reloads. Probably cause the same audiofiles are used as in Task 1, which works. As far as I checked, that is the only place with such weirdness. The short audio pieces are only in starting lessons, so I am kind of satisfied with it.

